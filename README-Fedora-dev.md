# Some notes to develop on Fedora

## Dependencies

```
sudo dnf install ...

xmltoman  vulkan-loader-devel  jack-audio-connection-kit-devel 
SDL2-devel 
cmake  
bluez*-devel  \
gstreamer1-plugins-base-devel gstreamer1-devel  sbc-devel libsndfile-devel 
libv4l-devel 
pulseaudio-libs-devel \
systemd-devel meson 
```

```
# That was Jack dev:
# readline-devel opus-devel ncurses-devel libsndfile-devel libsamplerate-devel libffado-devel libdb-devel doxygen
```

## Prerequisites

`dnf group install "C Development Tools and Libraries"`
